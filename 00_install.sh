#!/bin/bash

cd "$(dirname "$0")" && . ./10_variables.sh
pwd
cd "$(dirname "$0")" && . ./20_apache.sh
pwd
cd "$(dirname "$0")" && . ./30_php.sh
pwd
cd "$(dirname "$0")" && . ./40_nginx_pagespeed.sh
pwd
cd "$(dirname "$0")" && . ./50_mariadb.sh
pwd
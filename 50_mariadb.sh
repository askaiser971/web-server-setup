#!/bin/bash

# repo
set -xe \
    && apt-get install -y software-properties-common \
    && apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xcbcb082a1bb943db \
    && add-apt-repository "deb [arch=amd64,i386] http://de-rien.fr/mariadb/repo/${MARIADB_VERSION}/ubuntu trusty main"

# install
set -xe \
    && echo "mariadb-server-$MARIADB_VERSION mysql-server/root_password password $MARIADB_ROOT_PWD" \
    && debconf-set-selections <<< "mariadb-server-$MARIADB_VERSION mysql-server/root_password password $MARIADB_ROOT_PWD" \
    && debconf-set-selections <<< "mariadb-server-$MARIADB_VERSION mysql-server/root_password_again password $MARIADB_ROOT_PWD" \
    && debconf-set-selections <<< "mariadb-$MARIADB_VERSION mysql-server/root_password password $MARIADB_ROOT_PWD" \
    && debconf-set-selections <<< "mariadb-$MARIADB_VERSION mysql-server/root_password_again password $MARIADB_ROOT_PWD" \
    && debconf-set-selections <<< "maria-db-server-$MARIADB_VERSION mysql-server/root_password password $MARIADB_ROOT_PWD" \
    && debconf-set-selections <<< "maria-db-server-$MARIADB_VERSION mysql-server/root_password_again password $MARIADB_ROOT_PWD" \
    && debconf-set-selections <<< "maria-db-$MARIADB_VERSION mysql-server/root_password password $MARIADB_ROOT_PWD" \
    && debconf-set-selections <<< "maria-db-$MARIADB_VERSION mysql-server/root_password_again password $MARIADB_ROOT_PWD" \
    && apt-get update \
    && apt-get install -y mariadb-server

# run
set -xe \
    && service mysql start \
    && mysql --silent --force -uroot -p$MARIADB_ROOT_PWD < "extras/mysql_secure_installation.sql"
#!/bin/bash

BUILD_DEPS="ca-certificates curl bzip2 gcc libpcre++-dev libssl-dev make"

PATH="$PATH:$HTTPD_PREFIX/bin"

groupadd -r www-data && useradd -r --create-home -g www-data www-data

mkdir -p "$HTTPD_PREFIX" && chown www-data:www-data "$HTTPD_PREFIX"
cd $HTTPD_PREFIX

apt-get update && apt-get install -y --no-install-recommends libapr1 libaprutil1 libapr1-dev libaprutil1-dev libpcre++0 libssl1.0.0 && rm -r /var/lib/apt/lists/*

set -x \
	&& apt-get update \
	&& apt-get install -y --no-install-recommends $BUILD_DEPS \
	&& rm -r /var/lib/apt/lists/* \
	&& curl -fSL "$HTTPD_BZ2_URL" -o httpd.tar.bz2 \
	&& curl -fSL "$HTTPD_BZ2_URL.asc" -o httpd.tar.bz2.asc \
	&& export GNUPGHOME="$(mktemp -d)" \
	&& gpg --keyserver ha.pool.sks-keyservers.net --recv-keys A93D62ECC3C8EA12DB220EC934EA76E6791485A8 \
	&& gpg --batch --verify httpd.tar.bz2.asc httpd.tar.bz2 \
	&& rm -r "$GNUPGHOME" httpd.tar.bz2.asc \
	&& mkdir -p src/httpd \
	&& tar -xvf httpd.tar.bz2 -C src/httpd --strip-components=1 \
	&& rm httpd.tar.bz2 \
	&& cd src/httpd \
	&& ./configure --enable-so --enable-ssl --with-mpm=event --prefix=$HTTPD_PREFIX --enable-mods-shared=most \
	&& make -j"$(nproc)" \
	&& make install \
	&& cd ../../ \
	&& rm -r src/httpd \
	&& apt-get purge -y --auto-remove $BUILD_DEPS \
    && chown -R www-data:www-data "$HTTPD_PREFIX"

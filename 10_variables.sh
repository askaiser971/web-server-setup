#!/bin/bash

# Apache
HTTPD_VERSION="2.4.20"
HTTPD_PREFIX="/usr/local/apache2"
HTTPD_BZ2_URL="https://www.apache.org/dist/httpd/httpd-$HTTPD_VERSION.tar.bz2"

# PHP
PHP_VERSION="7.0.5"
PHP_FILENAME="php-7.0.5.tar.xz"
PHP_INI_DIR="/usr/local/etc/php"
PHP_SHA256="c41f1a03c24119c0dd9b741cdb67880486e64349fc33527767f6dc28d3803abb"

# NGINX & Pagespeed
NPS_VERSION="1.11.33.0"
NGINX_VERSION="1.8.1"

# MariaDB
MARIADB_VERSION="10.1"
MARIADB_ROOT_PWD="RmWJyJWd"